/**
 * Main router which inturn routes to specific routes
 */

import { Router } from 'express';

import dummyController from './dummy';
import userController from './user';
const router: Router = Router();

router.use('/dummy', dummyController);
router.use('/user', userController);

export default router;