/**
 * Controller for User API
 */

import { Router, Request, Response, NextFunction } from 'express';

import { UserService } from './../../../services/user';

const router: Router = Router();

/**
 * Get user Data
 * @returns { UserService.getUserData() }
 */
router.get('/:email', async (req: Request, res: Response, next: NextFunction) => {
    const { email } = req.params;
    
    try {
        res.json(await UserService.getUserData(email));
    } catch (err) {
        next(err);
    }
});

export default router;