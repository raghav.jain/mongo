import { Schema,model} from 'mongoose';

const UserSchema:Schema = new Schema({

    firstname: {type:String},
    lastname: {type:String},
    email: {type:String},
    phone: {type:String},
    rentedBooks: {
        type: Array
    },
    address: {
        type: String
    }
});

export default model("Users",UserSchema)



