export interface Usertype{
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    rentedBooks: String[],
    address: String 
}

export interface Addtype{
    addressId: String,
    street: String,
    postalCode: Number,
    city: String,
    country: String,
    house: String   
}

export interface Booktype{
    bookId: String,
    isbn: String,
    title: String,
    subtitle: String,
    author: String,
    published: String,
    publisher: String,
    pages: Number,
    description: String,
    website: String
}

export interface Rbdata{
    isbn: String,
    title: String,
    subtitle: String,
    author: String
}

export interface Result{
    name: String,
    email: String,
    phone: String
    address?: String,
    country?: String,
    rentedBooks?: Rbdata[]
}
