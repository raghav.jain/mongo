import { DocumentQuery, Document } from 'mongoose'
import Addresses from './../../model/address'
import Books from './../../model/book'
import Users from './../../model/user'
import { Addtype, Booktype, Result, Usertype } from './../user/Interface/interface'

/**
 * Service class and methods for user APIs
 */
class UserService {

    /**
     * Method to get info of a user
     * @param {string} email
     * @returns { Promise<Result> }
     */
     public static async getUserData(email: String): Promise<Result> {
        try {
                const user: Usertype = await Users.findOne(
                    {
                        email: email
                    }
                ).lean()
                if (!user) {
                    throw new Error('Provide correct e-mail')
                }   
                const address:DocumentQuery<Document | null, Document>= Addresses.findOne(
                    {
                        addressId: user.address
                    }
                )
                const books: DocumentQuery<Document[], Document>= Books.find(
                    {
                        bookId: user.rentedBooks
                    },
                    {
                        _id: 0,
                        isbn: 1,
                        title: 1,
                        subtitle: 1,
                        author: 1
                    }
                )
                const details: [ Addtype, Booktype[] ] = await Promise.all([address.lean(),books.lean()])
                const userAddress: Addtype = details[0]
                const rentedBooks: Booktype[] = details[1]
                const response: Result = {
                    name: user.firstname +' '+ user.lastname,
                    email: user.email,
                    phone: user.phone,
                }
                if (userAddress) {
                    response.address = userAddress.house +' '+ userAddress.street +' '+ userAddress.city +' '+ userAddress.postalCode;
                    response.country = userAddress.country;
                }
                if (rentedBooks.length > 0) {
                    response.rentedBooks = rentedBooks
                }
                return response
            }
        catch (err) {
            throw err
        }
    }
}
export { UserService };
